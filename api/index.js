const express = require('express');
const app = express();

// Endpoint de prueba
app.get('/', (req, res) => {
  res.send('¡Hola, mundo!');
});

// Puerto en el que escucha el servidor
const port = 6000;

// Inicia el servidor
app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});
