// Función que realiza una operación asíncrona y llama al callback cuando termina
function asyncOperation(callback) {
    console.log('Iniciando operación asíncrona');
    setTimeout(function () {
        const result = 42; // Simulación de un resultado
        callback(result);
    }, 2000);
}

// Función de callback que maneja el resultado de la operación asíncrona
function handleResult(result) {
    console.log('El resultado es:', result);
}

// Utilizamos la función asyncOperation pasando el callback handleResult
asyncOperation(handleResult);
