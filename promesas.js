// Creamos una promesa sencilla que se resuelve después de 1 segundo
const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('¡Promesa resuelta!');
  }, 1000);
//   throw new Error('Error en la promesa');
});

// Utilizamos la promesa
// console.log('antes de ejecutar la promesa');

myPromise
.then((result) => {
    console.log(result); // Imprime: ¡Promesa resuelta!
})
.catch((error) => {
    console.log('ERROR PROMESA',error);
}).finally(() => {
    // console.log('Esto se ejecuta siempre');
});

// console.log('despues de ejecutar la promesa');